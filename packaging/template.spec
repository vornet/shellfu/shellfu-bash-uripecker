%global sfincb %{_datadir}/shellfu/include-bash
%global sfmodn __SHELLFU_MODNAME__
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__
%if 0%{?rhel} && 0%{?rhel} <= 7
%global py_req     python
%global pylibs_req python-libs
%else
%global py_req     python3
%global pylibs_req python3-libs
%endif

Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        LGPLv2
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  %shellfu_req
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  %py_req
BuildRequires:  %pylibs_req
BuildRequires:  shellfu-bash-pretty

Requires: %shellfu_req
Requires: perl
Requires: %py_req
Requires: %pylibs_req
Requires: shellfu-bash
Requires: shellfu-bash-pretty
%description
__SHELLFU_MODDESC__

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr

%check
make test \
    SHELLFU_PATH=%{buildroot}/%{_datadir}/shellfu/include-bash

%files
%doc %{_docdir}/%{name}/README.md
%{sfincb}/%{sfmodn}.sh


%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
